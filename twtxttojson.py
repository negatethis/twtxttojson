import json

def readFile():
    with open('../content/twtxt/twtxt.txt', 'r') as twtxt_file:
        tweets = twtxt_file.readlines()
    return tweets

def makeJSONList(tweets):
    json_to_load = []
    for tweet in tweets:
        split = tweet.split("\t")
        date = split[0]
        content = split[1]
        dictionary = { 'date': date, 'content': content }
        json_to_load.append(dictionary)
    return json_to_load

def loadJSONToFile(json_to_load):
    with open('../data/twtxt.json', 'w') as twtxt_json:
        json.dump(json_to_load, twtxt_json)

def main():
    tweets = readFile()
    json_to_load = makeJSONList(tweets)
    json_to_load.reverse()
    loadJSONToFile(json_to_load)

if __name__ == "__main__":
    main()
